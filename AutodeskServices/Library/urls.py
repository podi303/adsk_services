from django.conf.urls import url
from Library import views

urlpatterns = [
    url(r'^$', views.about, name='index'),
    url(r'^index/', views.about, name='index'),
    url(r'^books/', views.books, name='books'),
    url(r'^requestBook/', views.requestBook, name='requestBook'),
    url(r'^add/', views.add, name='add'),
    url(r'^contact/', views.contact, name='contact'),
    url(r'^details/', views.details, name='details'),
    url(r'^addAdmin/', views.addAdmin, name='addAdmin'),
]
