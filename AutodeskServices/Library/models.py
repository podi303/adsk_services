from django.db import models
from django.template.defaultfilters import slugify

from django.core.files.storage import FileSystemStorage

fs = FileSystemStorage(location='/static/images/gallery')

class book(models.Model):
    name = models.CharField(max_length=128, unique=True)
    authors = models.CharField(max_length=128)
    language = models.CharField(max_length=128)
    otherDetails = models.URLField()
    availableCopies = models.IntegerField()
    borrowUntilDate = models.DateField()
    image = models.ImageField(storage=fs)
    commentName = models.CharField(max_length=128)
    commentText = models.CharField(max_length=2048)
    slug = models.SlugField(unique=True,blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(book, self).save(*args, **kwargs)


    def __str__(self):
        return self.name

class logIn(models.Model):
    adminName = models.CharField(max_length=128)
    adminPass = models.CharField(max_length=128)

    class Meta:
        verbose_name_plural = 'login'

        # def __str__(self):
        #     return self.adminName




class requestBook(models.Model):
    userName = models.CharField(max_length=128)
    email = models.EmailField(max_length=128)
    website = models.URLField()

    # def __str__(self):
    #     return self.userName

class contact(models.Model):
    userName = models.CharField(max_length=128)
    email = models.EmailField(max_length=128)
    comment = models.CharField(max_length=1024)

    class Meta:
        verbose_name_plural = 'contact'

    def __str__(self):
        return self.userName