from django.http import HttpResponse
from django.shortcuts import render
from Library.models import book

def about(request):
    books_list = book.objects.all() # order_by('availableCopies')[:2]
    context_dict = {'books':books_list}
    return render(request, 'Library/index.html', context_dict)

def books(request):
    return render(request, 'Library/books.html')

def requestBook(request):
    return render(request, 'Library/requestBook.html')

def add(request):
    return render(request, 'Library/add.html')

def contact(request):
    return render(request, 'Library/contact.html')

def details(request):
    return render(request, 'Library/details.html')

def addAdmin(request):
    return render(request, 'Library/addAdmin.html')
