# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-28 22:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, unique=True)),
                ('authors', models.CharField(max_length=128)),
                ('language', models.CharField(max_length=128)),
                ('otherDetails', models.URLField()),
                ('availableCopies', models.IntegerField()),
                ('borrowUntilDate', models.DateField()),
                ('image', models.ImageField(upload_to='')),
                ('commentName', models.CharField(max_length=128)),
                ('commentText', models.CharField(max_length=2048)),
            ],
        ),
        migrations.CreateModel(
            name='contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('userName', models.CharField(max_length=128)),
                ('email', models.EmailField(max_length=128)),
                ('comment', models.CharField(max_length=1024)),
            ],
        ),
        migrations.CreateModel(
            name='requestBook',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('userName', models.CharField(max_length=128)),
                ('email', models.EmailField(max_length=128)),
                ('website', models.URLField()),
            ],
        ),
    ]
