from django.contrib import admin
from Library.models import book, logIn, requestBook, contact

class BookAdmin(admin.ModelAdmin):
        list_display = ('name', 'authors', 'availableCopies')
        prepopulated_fields = {'slug':('name',)}

class logInAdmin(admin.ModelAdmin):
    list_display = ('adminName','adminPass')

admin.site.register(book, BookAdmin)
admin.site.register(logIn, logInAdmin)
admin.site.register(requestBook)
admin.site.register(contact)

